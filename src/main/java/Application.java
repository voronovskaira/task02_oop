import controller.Controller;
import controller.ControllerInterface;
import view.ConsoleView;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        ControllerInterface app = new Controller(new ConsoleView());
        String menuPoint = "";
        do {
            System.out.println("Menu\n1 - sort minerals by price\n2 - get minerals by clarity\n3 - create necklace\n0 - exit");
            menuPoint = scan.nextLine();
            switch (menuPoint) {
                case "1":
                    app.sortByPrice();
                    break;
                case "2":
                    app.getGemsByClarity();
                    break;
                case "3":
                    app.createNecklace();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Wrong value, please input another one");
            }
        } while (!menuPoint.equals("0"));


    }
}

