package controller;

import model.Necklace;

public interface ShopInterface {
    Necklace selectMineral();

    void getGemsByClarity(int start, int end);

    void sortByPrice();
}
