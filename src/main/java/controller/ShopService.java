package controller;

import model.Mineral;
import model.Necklace;
import model.Shop;
import view.View;

import java.util.*;

public class ShopService implements ShopInterface {
    private Shop shop = new Shop();
    private Scanner scanner = new Scanner(System.in);
    private View view;

    ShopService(View view) {
        this.view = view;
    }

    @Override
    public Necklace selectMineral() {
        int gemCount = 0;
        Necklace necklace = new Necklace();
        view.print("Select 5 mineral from list above to create your necklace\n");
        while (gemCount < 5) {
            view.print("Select please");
            for (Map.Entry<Integer, Mineral> mineralEntry : shop.getMinerals().entrySet()) {
                view.print(mineralEntry.getKey() + " - " + mineralEntry.getValue().getName());
            }
            int gemIndex = 0;
            do {
                try {
                    gemIndex = scanner.nextInt();
                } catch (InputMismatchException e) {
                    view.print("You have inputted wrong value please input correct value from range 1 to 9");
                }
                scanner.nextLine();
            } while (!(gemIndex > 0 && gemIndex < 10));
            view.print("You have selected gem - " + shop.getMinerals().get(gemIndex));
            necklace.addMineral(shop.getMinerals().get(gemIndex));
            gemCount++;

        }
        return necklace;
    }

    @Override
    public void getGemsByClarity(int start, int end) {
        List<Mineral> minerals = new ArrayList<>();
        for (Mineral mineral : shop.getMinerals().values()) {
            if (mineral.getClarity() >= start && mineral.getClarity() <= end) {
                minerals.add(mineral);
            }
        }
        if (minerals.isEmpty()) {
            view.print("No such minerals in this range");
        } else {
            view.print("Minerals in this range : ");
            for (Mineral min : minerals)
                view.print(min.toString() + " clarity = " + min.getClarity());
        }

    }

    @Override
    public void sortByPrice() {
        List<Mineral> minerals = new ArrayList<>(shop.getMinerals().values());
        Collections.sort(minerals);
        for (Mineral mineral : minerals)
            view.print(mineral.toString() + " price = " + mineral.getPrice() + "$");


    }
}
