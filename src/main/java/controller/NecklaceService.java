package controller;

import model.Mineral;
import model.Necklace;
import view.View;

public class NecklaceService implements NecklaceInterface {
    private Necklace necklace;
    private View view;

    public NecklaceService(View view) {
        this.view = view;
    }

    @Override
    public void calculateWeightOfNecklace() {
        double weightSum = 0;
        for (Mineral mineral : necklace.getNecklace()) {
            weightSum += mineral.getWeight();
        }
        view.print("Weight of necklace " + weightSum + " carat");
    }

    @Override
    public void calculatePriceOfNecklace() {
        double priceSum = 0;
        for (Mineral mineral : necklace.getNecklace()) {
            priceSum += mineral.getPrice();
        }
        view.print("Price of your necklace : " + priceSum + "$");
    }

    @Override
    public void addNecklace(Necklace necklace) {
        this.necklace = necklace;
    }
}
