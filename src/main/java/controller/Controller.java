package controller;

import model.Necklace;
import view.View;

import java.util.Scanner;

public class Controller implements ControllerInterface {
    private NecklaceInterface necklaceInterface;
    private ShopInterface shopInterface;
    private Scanner scanner = new Scanner(System.in);
    private View view;

    public Controller(View view) {
        this.view = view;
        this.shopInterface = new ShopService(view);
        this.necklaceInterface = new NecklaceService(view);
    }

    public void sortByPrice() {
        shopInterface.sortByPrice();
    }

    public void createNecklace() {
        Necklace necklace = shopInterface.selectMineral();
        necklaceInterface.addNecklace(necklace);
        view.print("You have created such necklace");
        view.print(necklace.toString());
        necklaceInterface.calculatePriceOfNecklace();
        necklaceInterface.calculateWeightOfNecklace();
    }

    @Override
    public void getGemsByClarity() {
        view.print("Please input start of range");
        int start = scanner.nextInt();
        view.print("Please input end of range");
        int end = scanner.nextInt();
        shopInterface.getGemsByClarity(start, end);
    }
}
