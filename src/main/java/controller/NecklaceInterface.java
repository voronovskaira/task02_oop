package controller;

import model.Necklace;

public interface NecklaceInterface {
    void calculateWeightOfNecklace();

    void calculatePriceOfNecklace();

    void addNecklace(Necklace necklace);

}
