package controller;

public interface ControllerInterface {
    void sortByPrice();

    void createNecklace();

    void getGemsByClarity();
}
