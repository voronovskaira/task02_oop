package view;

public class ConsoleView implements View {
    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public void print(double value) {
        System.out.println(value);
    }
}
