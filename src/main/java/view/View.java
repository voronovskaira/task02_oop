package view;

public interface View {
    void print(String message);

    void print(double value);
}
