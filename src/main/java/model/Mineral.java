package model;

public class Mineral implements Comparable<Mineral> {
    private String name;
    private String color;
    private int clarity;
    private double weight;
    private double price;

    Mineral(String name, String color, int clarity, double weight, double price) {
        this.name = name;
        this.color = color;
        this.clarity = clarity;
        this.weight = weight;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getWeight() {
        return weight;
    }

    public int getClarity() {
        return clarity;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Mineral o) {
        return (int) (this.price - o.price);
    }
}
