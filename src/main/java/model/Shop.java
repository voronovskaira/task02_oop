package model;

import java.util.HashMap;
import java.util.Map;

public class Shop {
    private Map<Integer, Mineral> minerals = new HashMap<>();

    {
        minerals.put(1, new Mineral("Sapphire", "Blue", 4, 0.27, 83));
        minerals.put(2, new Mineral("Ruby", "Red", 5, 0.18, 170));
        minerals.put(3, new Mineral("Emerald", "Green", 4, 0.3, 100));
        minerals.put(4, new Mineral("Alexandrite", "Purple", 3, 0.2, 98));
        minerals.put(5, new Mineral("Amethyst", "Purple", 4, 2.3, 92));
        minerals.put(6, new Mineral("Morion", "Black", 9, 2.0, 70));
        minerals.put(7, new Mineral("Citrine", "Yellow", 5, 4.0, 65));
        minerals.put(8, new Mineral("Lazurite", "Blue", 6, 4.5, 40));
        minerals.put(9, new Mineral("Diamond", "Pink", 2, 0.1, 280));
    }

    public Map<Integer, Mineral> getMinerals() {
        return minerals;
    }
}
