package model;

import java.util.ArrayList;
import java.util.List;

public class Necklace {
    private List<Mineral> necklace = new ArrayList<>();

    public List<Mineral> getNecklace() {
        return necklace;
    }

    public void addMineral(Mineral mineral) {
        necklace.add(mineral);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Mineral mineral : necklace)
            stringBuilder.append("\n").append(mineral);
        return stringBuilder.toString();
    }
}
